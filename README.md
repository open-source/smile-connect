<p align="center">
  <img src="https://smile.unicaen.fr/img/logo_smile_RVB.jpg" alt="Logo de votre entreprise" width="300">
  <img src="https://api-platform.com/images/logos/Logo_Circle%20webby%20text%20blue.png" alt="Logo d'API Platform" width="100">
</p>

# Table des matières

- [Smile-connect](#smile-connect)
- [Fonctionnalités](#fonctionnalités)
- [Pré-requis](#pré-requis)
- [Installation](#installation)
    - [1. Cloner le dépôt GitHub](#1-cloner-le-dépôt-github)
    - [2. Configurer l'application avec le fichier `.env`](#2-configurer-lapplication-avec-le-fichier-env)
    - [3. Configurer vos requêtes SQL dans /data pour l'import d'APOGEE](#3-configurer-vos-requêtes-sql-dans-data-pour-limport-dapogee)
        - [Documentation d'importation de données](#documentation-dimportation-de-données)
        - [Fichiers SQL](#fichiers-sql)
        - [Propriétés des données](#propriétés-des-données)
        - [Exemple](#exemple)
    - [4. Lancer le docker](#4-lancer-le-docker)
- [Utilisation](#utilisation)
    - [Créer et supprimer des utilisateurs](#créer-et-supprimer-des-utilisateurs)
    - [Importer les données APOGEE dans smile-connect](#importer-les-données-apogee-dans-smile-connect-manuellement)
- [Autres](#autres)
- [Contribution](#contribution)

# Smile-connect

Smile-connect est une application conçue pour récupérer l'offre de formation d'une base de données APOGEE locale d'une université afin de fournir l'offre de formation à l'application [Smile](https://github.com/EsupPortail/esup-smile), une application de gestion de mobilité étudiante.

## Fonctionnalités

- Récupération des données de la base de données APOGEE
- Fournit l'offre de formation à l'application Smile

## Pré-requis

- Docker
- Serveur ouvert vers internet sur le port 443 et un accès à APOGEE

## Installation

### 1. Cloner le dépôt GitHub
    
```bash
git clone git@git.unicaen.fr:open-source/smile-connect.git
```
Le serveur doit avoir accès à APOGEE, le port 443 doit être ouvert vers l'extérieur dans le cas d'une installation SAAS de Smile.

### 2. Configurer l'application avec le fichier `.env`

Copier le fichier `.env.dist` en `.env` et configurer les variables d'environnement.

```bash
cp .env.dist .env
```
```bash
# .env.dist

# Le nom de domaine de smile-connect
SERVER_NAME=example.com

# Si vous utilisez un proxy, vous pouvez le définir ici
HTTP_PROXY=
HTTPS_PROXY=

# Environnement de l'application production ou dev
APP_ENV=production
# Symfony secret, vous pouvez générer un secret avec `echo $(openssl rand -hex 16)`
APP_SECRET=

# Liste des domaines de confiance, remplacez example.com par votre nom de domaine et celui de smile si il est différent
TRUSTED_HOSTS="^localhost$","^example.com$"
# Liste des proxies de confiance, notez l'IP de votre proxy si vous en avez un
TRUSTED_PROXIES=

# Informations de connexion à la base de donnée de l'api, vous pouvez modifier les valeurs à votre convenance
POSTGRES_DB=smile-connect
POSTGRES_PASSWORD=
POSTGRES_USER=
# Construit avec les informations ci-dessus, ne pas modifier
DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@database:5432/${POSTGRES_DB}?serverVersion=16&charset=utf8"

# Informations de connexion à la base de donnée Oracle APOGEE
ORACLE_HOST=
ORACLE_PORT=
# DBNAME = SID = SERVICE_NAME
ORACLE_DBNAME=
ORACLE_USER=
ORACLE_PASSWORD=

# Informations de connexion à l'API via Basic Auth, à communiquer à Smile
API_USER=
API_PASSWORD=
```
### 3. Configurer vos requêtes SQL dans /data pour l'import d'APOGEE

#### Documentation d'importation de données

Ce document décrit les exigences pour que l'importation de données fonctionne correctement dans le fichier `ImportDataCommand.php`.

#### Fichiers SQL

L'importation de données repose sur plusieurs fichiers SQL :

- `api/data/request_composantes.sql` : Ce fichier doit contenir des requêtes SQL pour récupérer les données de composantes de la base de données APOGEE.
- `api/data/request_formation.sql` : Ce fichier doit contenir des requêtes SQL pour récupérer les données de formations de la base de données APOGEE.
- `api/data/request_cour.sql` : Ce fichier doit contenir des requêtes SQL pour récupérer les données de cours de la base de données APOGEE.

Il est préférable pour la visibilité de Smile de n'importer qu'un catalogue de cour utilisé en mobilité et pas l'intégralité de l'offre de formation.


#### Propriétés des données

Les données récupérées d'APOGEE doivent avoir les propriétés suivantes :

Note : Les propriétés non obligatoires peuvent être omises, mais il est préférable d'en inclure autant que possible.

#### Données de composantes

| Nom            | Type     | Obligatoire | Commentaires                 | Exemple      |
|----------------|----------|-------------|------------------------------|--------------|
| `COD_CMP`      | `string` | Oui         | identifiant de la composante |              |
| `LIB_CMP`      | `string` | Oui         | Libelle                      |              |
| `LIB_LONG_CMP` | `string` | Non         | Libelle Long                 |              |
| `LIC_CMP`      | `string` | Non         | Acronyme/Abréviation         | STAPS, DROIT |

#### Données de formations

| Nom                      | Type     | Obligatoire | Commentaires    | Exemple                                |
|--------------------------|----------|-------------|-----------------|----------------------------------------|
| `COD_VET`                | `string` | Oui         | Code formation  |                                        |
| `COD_CMP`                | `string` | Oui         | Code composante |                                        |
| `LIB_WEB_VET`            | `string` | Non         | Libelle         |                                        |
| `TYPE_FORMATION`         | `string` | Non         | Caractère(1)    | C ou D, pour Certifiante ou Diplomante |
| `LIBELLE_TYPE_FORMATION` | `string` | Non         |                 | Licence, Master                        |
| `NIVEAU_FORMATION`       | `int`    | Non         |                 |                                        |

#### Données de cours

| Nom                   | Type     | Obligatoire | Commentaires                 |
|-----------------------|----------|-------------|------------------------------|
| `COD_ELP`             | `string` | Oui         | Code cour                    |
| `COD_CMP`             | `string` | Oui         | Code composante              |
| `COD_VET`             | `string` | Oui         | Code formation               |
| `LIB_ELP`             | `string` | Oui         | Libelle cour                 |
| `NBR_CRD_ELP`         | `string` | Oui         | Crédits                      |
| `LIB_CMT_ELP`         | `string` | Non         | Libelle/Commentaires anglais |
| `S1`                  | `string` | Non         | "S1" ou vide ou boolean      |
| `S2`                  | `string` | Non         | "S2" ou vide ou boolean      |
| `LANGUE_ENSEIGNEMENT` | `string` | Non         | Français, Anglais, etc       | |
| `NBR_VOL_ELP`         | `string` | Non         | Volume horaire               |

Assurez-vous que les requêtes SQL dans les fichiers SQL récupèrent des données avec ces propriétés.
#### Exemple

Voici un exemple de requête SQL pour récupérer les données de composantes :

```sql
-- request_composantes.sql
SELECT
    COD_CMP,
    LIB_CMP,
    LIB_WEB_CMP as LIB_LONG_CMP,
    LIC_CMP
FROM
    composante
```

Le plus simple étant de créer une vue dans APOGEE puis simplement de l'appeler, exemple à Caen:

```sql
-- request_cour.sql
-- WELCOME_CATALOGUE est une vue créée dans APOGEE
SELECT * FROM WELCOME_CATALOGUE
```

### 4. Lancer le docker

Le docker intègre un serveur apache pour récupérer les requêtes, vous pouvez l'utiliser tel quel en changeant le port 8444 par le port 443 dans `compose.yaml`, ou utiliser un reverse proxy pour rediriger les requêtes vers le port 8444.
```yaml
# compose.yaml
apache:
    ports:
    - 8444:443
    # ou
    - 443:443
```

Les fichiers SSL sont à mettre dans `server_conf/ssl`, les noms par defaults sont `server.crt` et `server.key`, si vous changez les noms, il faudra modifier le fichier `server_conf/vhosts.conf` pour les adapter.

```bash
docker compose build
```
puis
```bash
docker compose up -d
```
Pendant le compsoe up, les migrations de la base de données seront effectuées.

### 5. Mettre en place une tâche cron pour l'importation des données

Pour mettre en place une tâche cron pour l'importation des données, vous pouvez ajouter une tâche cron avec la commande suivante :

```bash
crontab -e
```

```bash
# Importe les données APOGEE toutes les nuits à 00h
0 0 * * * docker exec smile-connect-php bin/console app:import-data
```

## Utilisation

Une fois l'application lancée, vous pouvez accéder à l'API via `https://servername/`.
Une authentification Basic Auth est nécessaire, les identifiants sont ceux définis dans le fichier `.env` API_USER et API_PASSWORD.

### Créer et supprimer des utilisateurs

Vous pouvez ajouter ou supprimer des utilisateurs avec la commande `bin/console app:create-user` et `bin/console app:delete-user` respectivement.

```bash
# Créer un utilisateur
docker exec -it smile-connect-php bin/console app:create-user username password

# Supprimer un utilisateur
docker exec smile-connect-php bin/console app:delete-user username
```

### Importer les données APOGEE dans smile-connect manuellement

Pour importer les données APOGEE dans smile-connect, vous pouvez utiliser la commande `bin/console app:import-data` si vous avez configuré les requêtes.

```bash
docker exec smile-connect-php bin/console app:import-data
```

### Autres

Vous pouvez directement mettre à jour les tables de la base Postgresql avec vos propres moyens pour rendre accessible votre ODF à l'API.

## Contribution

Les contributions sont les bienvenues ! Veuillez créer une issue ou une pull request pour toute contribution que vous souhaitez apporter.
