<?php

namespace App\Tests\Api;

use ApiPlatform\Symfony\Bundle\Test\ApiTestCase;

class ComposanteTest extends ApiTestCase
{
    public function testGetComposantes(): void
    {
        $response = static::createClient()->request('GET', '/composantes');

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            '@context' => '/contexts/Composante',
            '@id' => '/composantes',
            '@type' => 'hydra:Collection',
        ]);
    }
}
