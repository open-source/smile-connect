<?php
// api/src/Entity/Formation.php
namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\State\DbImportCollectionStateProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/formations/dbimport',
            provider: DbImportCollectionStateProvider::class,
        ),
    ],
    normalizationContext: ['groups' => ['read']],
)]
#[GetCollection]
class Formation
{
    #[Groups('read')]
    #[ORM\Id, ORM\Column, ORM\GeneratedValue('SEQUENCE')]
    private ?int $id = null;
    #[Groups('read')]
    #[ORM\Column(type: "string", length: 3)]
    private string $COD_CMP;
    #[Groups('read')]
    #[ORM\Column(type: "string", length: 47)]
    private string $COD_VET;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 120, nullable: true)]
    private ?string $LIB_WEB_VET = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 1, nullable: true)]
    private ?string $TYPE_FORMATION = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 7, nullable: true)]
    private ?string $LIBELLE_TYPE_FORMATION = null;

    #[Groups('read')]
    #[ORM\Column(type: "integer", nullable: true)]
    private ?int $NIVEAU_FORMATION = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCmp(): ?string
    {
        return $this->COD_CMP;
    }

    public function getCodVet(): ?string
    {
        return $this->COD_VET;
    }

    public function getLibWebVet(): ?string
    {
        return $this->LIB_WEB_VET;
    }

    public function getTypeFormation(): ?string
    {
        return $this->TYPE_FORMATION;
    }

    public function getLibelleTypeFormation(): ?string
    {
        return $this->LIBELLE_TYPE_FORMATION;
    }

    public function getNiveauFormation(): ?int
    {
        return $this->NIVEAU_FORMATION;
    }
}
