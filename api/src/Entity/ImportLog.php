<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
class ImportLog
{
    #[ORM\Id, ORM\GeneratedValue('SEQUENCE'), ORM\Column(type: "integer")]
    private int $id;

    #[ORM\Column(type: "datetime")]
    private \DateTimeInterface $date;

    #[ORM\Column(type: "string", length: 255)]
    private string $status;

    #[ORM\Column(type: "text", nullable: true)]
    private string $message;

    #[ORM\Column(type: "integer", nullable: true)]
    private int $nbComposantes;

    #[ORM\Column(type: "integer", nullable: true)]
    private int $nbFormations;

    #[ORM\Column(type: "integer", nullable: true)]
    private int $nbCours;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNbComposantes(): ?int
    {
        return $this->nbComposantes;
    }

    public function setNbComposantes(int $nbComposantes): self
    {
        $this->nbComposantes = $nbComposantes;

        return $this;
    }

    public function getNbFormations(): ?int
    {
        return $this->nbFormations;
    }

    public function setNbFormations(int $nbFormations): self
    {
        $this->nbFormations = $nbFormations;

        return $this;
    }

    public function getNbCours(): ?int
    {
        return $this->nbCours;
    }

    public function setNbCours(int $nbCours): self
    {
        $this->nbCours = $nbCours;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
