<?php
// api/src/Entity/Cour.php
namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\State\DbImportCollectionStateProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/cours/dbimport',
            provider: DbImportCollectionStateProvider::class,
        ),
    ],
    normalizationContext: ['groups' => ['read']],
)]
#[GetCollection]
class Cour
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue('SEQUENCE')]
    private ?int $id = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 8)]
    private string $COD_ELP;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 3)]
    private string $COD_CMP;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 47)]
    private string $COD_VET;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 60)]
    private string $LIB_ELP;
    #[Groups('read')]
    #[ORM\Column(type: "string", length: 2000, nullable: true)]
    private ?string $LIB_CMT_ELP = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 2, nullable: true)]
    private ?string $S1 = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 2, nullable: true)]
    private ?string $S2 = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 17, nullable: true)]
    private ?string $LANGUE_ENSEIGNEMENT = null;

    #[Groups('read')]
    #[ORM\Column(type: "decimal")]
    private string $NBR_CRD_ELP;

    #[Groups('read')]
    #[ORM\Column(type: "decimal", precision:6, scale: 2,nullable: true,)]
    private ?string $NBR_VOL_ELP = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCmp(): ?string
    {
        return $this->COD_CMP;
    }

    public function getCodVet(): ?string
    {
        return $this->COD_VET;
    }

    public function getCodElp(): ?string
    {
        return $this->COD_ELP;
    }

    public function getLangueEnseignement(): ?string
    {
        return $this->LANGUE_ENSEIGNEMENT;
    }

    public function getLibElp(): ?string
    {
        return $this->LIB_ELP;
    }

    public function getLibCmtElp(): ?string
    {
        return $this->LIB_CMT_ELP;
    }

    public function getS1(): ?string
    {
        return $this->S1;
    }

    public function getS2(): ?string
    {
        return $this->S2;
    }

    public function getNbrCrdElp(): ?string
    {
        return $this->NBR_CRD_ELP;
    }

    public function getNbrVolElp(): ?string
    {
        return $this->NBR_VOL_ELP;
    }
}
