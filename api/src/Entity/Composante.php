<?php
// api/src/Entity/Composante.php
namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GetCollection;
use App\State\DbImportCollectionStateProvider;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity]
#[ApiResource(
    operations: [
        new GetCollection(
            uriTemplate: '/composantes/dbimport',
            provider: DbImportCollectionStateProvider::class
        ),
    ],
    normalizationContext: ['groups' => ['read']],
)]
#[GetCollection]
class Composante
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue('SEQUENCE')]
    private ?int $id = null;
    #[Groups('read')]
    #[ORM\Column(type: "string", length: 3)]
    private ?string $COD_CMP = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 40)]
    private ?string $LIB_CMP = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 120, nullable: true)]
    private ?string $LIB_LONG_CMP = null;

    #[Groups('read')]
    #[ORM\Column(type: "string", length: 40, nullable: true)]
    private ?string $LIC_CMP = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodCmp(): ?string
    {
        return $this->COD_CMP;
    }

    public function getLibCmp(): ?string
    {
        return $this->LIB_CMP;
    }

    public function getLibWebCmp(): ?string
    {
        return $this->LIB_WEB_CMP;
    }

    public function getLicCmp(): ?string
    {
        return $this->LIC_CMP;
    }
}
