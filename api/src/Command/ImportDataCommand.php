<?php
// src/Command/ImportDataCommand.php

namespace App\Command;

use App\Entity\Apodata;
use App\Entity\ImportLog;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;

class ImportDataCommand extends Command
{
    private Connection $connectionPsql;
    private Connection $connectionOracle;
    private EntityManagerInterface $entityManager;
    private string $projectDir;

    public function __construct(Connection $connectionPsql, Connection $connectionOracle, EntityManagerInterface $entityManager, KernelInterface $kernel)
    {
        $this->connectionPsql = $connectionPsql;
        $this->connectionOracle = $connectionOracle;
        $this->entityManager = $entityManager;
        $this->projectDir = $kernel->getProjectDir();

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
        ->setName('app:import-data')
        ->setDescription('Imports data from Oracle database');
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $commandStatus = Command::SUCCESS;
        $io = new SymfonyStyle($input, $output);
        $importLog = new ImportLog();
        $importLog->setDate(new \DateTime());
        $io->writeln($importLog->getDate()->format('Y-m-d H:i:s'));
        $io->title('Import des données depuis APOGEE');
        try {
            $this->import($io);
        } catch (\Exception $e) {
            $io->error('Error: ' . $e->getMessage());
            $importLog->setMessage($e->getMessage());
            $importLog->setStatus('FAILURE');
            $this->entityManager->persist($importLog);
            $this->entityManager->flush();
            return Command::FAILURE;
        }

        $nbComposantes = $this->entityManager->getRepository('App\Entity\Composante')->count([]);
        $nbFormations = $this->entityManager->getRepository('App\Entity\Formation')->count([]);
        $nbCours = $this->entityManager->getRepository('App\Entity\Cour')->count([]);

        $importLog->setNbComposantes($nbComposantes);
        $importLog->setNbFormations($nbFormations);
        $importLog->setNbCours($nbCours);
        $importLog->setStatus('SUCCESS');
        $this->entityManager->persist($importLog);
        $this->entityManager->flush();

        $io->success('Import terminé | Composantes: '.$nbComposantes.', Formations: '.$nbFormations.', Cours: '.$nbCours);
        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function import(SymfonyStyle $io): int
    {
        // Purge des tables avant l'import
        $io->info('Purge des tables avant l\'import');
        $this->purgeTables($io);

        // Import des composantes depuis APOGEE
        $io->info('Import des composantes depuis APOGEE : request_composante.sql');
        $this->importComposantes($io);

        // Import des formations depuis APOGEE
        $io->info('Import des formations depuis APOGEE : request_formation.sql');
        $this->importFormations($io);
        // Import du catalogue de cours depuis APOGEE
        $io->info('Import des cours depuis APOGEE : request_cour.sql');
        $this->importCours($io);

        // Remove unused composantes
        $io->info('Remove unused composantes');
        $this->removeUnused($io);

        //        $this->checkCourComposantes($io);
        //        $this->checkCourFormation($io);
        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function purgeTables(SymfonyStyle $io): int
    {
        $this->connectionPsql->executeStatement('TRUNCATE TABLE composante');
        $io->success('Table composante purgée');

        $this->connectionPsql->executeStatement('TRUNCATE TABLE cour');
        $io->success('Table cour purgée');

        $this->connectionPsql->executeStatement('TRUNCATE TABLE formation');
        $io->success('Table formation purgée');

        // reset les séquences
        $this->connectionPsql->executeStatement('ALTER SEQUENCE formation_id_seq RESTART WITH 1');
        $this->connectionPsql->executeStatement('ALTER SEQUENCE cour_id_seq RESTART WITH 1');
        $this->connectionPsql->executeStatement('ALTER SEQUENCE composante_id_seq RESTART WITH 1');
        $io->success('Séquences réinitialisées');

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function checkCourComposantes(SymfonyStyle $io): int
    {
        $query = "SELECT COUNT(*) FROM cour WHERE cod_cmp NOT IN (SELECT cod_cmp FROM composante)";
        $stmt = $this->connectionPsql->executeQuery($query);
        $result = $stmt->fetchOne();
        if ($result > 0) {
            $io->info('Il y a des cours sans composante associée');
        } else {
            $io->info('Tous les cours ont une composante associée');
        }

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function checkCourFormation(SymfonyStyle $io): int
    {
        $query = "SELECT COUNT(*) FROM cour WHERE cod_vet NOT IN (SELECT cod_vet FROM formation)";
        $stmt = $this->connectionPsql->executeQuery($query);
        $result = $stmt->fetchOne();
        if ($result > 0) {
            $io->info('Il y a des cours sans formation associée');
        } else {
            $io->info('Tous les cours ont une formation associée');
        }

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function removeUnused(SymfonyStyle $io): int
    {
        $query = "DELETE FROM composante WHERE composante.cod_cmp NOT IN (SELECT DISTINCT cour.cod_cmp FROM cour)";
        $deletedCount = $this->connectionPsql->executeStatement($query);
        $io->success($deletedCount . ' composantes non utilisées supprimées');

        $query = "DELETE FROM formation WHERE formation.cod_vet NOT IN (SELECT DISTINCT cour.cod_vet FROM cour)";
        $deletedCount = $this->connectionPsql->executeStatement($query);
        $io->success($deletedCount . ' formations non utilisées supprimées');

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function importComposantes(SymfonyStyle $io): int
    {
        $query = file_get_contents($this->projectDir.'/data/request_composante.sql');
        $stmt = $this->connectionOracle->prepare($query);
        $result = $stmt->executeQuery();
        $composanteList = $result->fetchAllAssociative();
        $io->success(count($composanteList).' composantes récupérées depuis APOGEE');

        try {
            $io->info('Insertion des composantes dans la base PSQL');

            $query = "INSERT INTO composante (id, COD_CMP, LIB_CMP, LIB_LONG_CMP, LIC_CMP)
            VALUES (nextval('composante_id_seq'), :COD_CMP, :LIB_CMP, :LIB_LONG_CMP, :LIC_CMP)";

            // Start the transaction
            $this->connectionPsql->beginTransaction();

            foreach ($composanteList as $line) {
                $line['LIB_LONG_CMP'] = $line['LIB_LONG_CMP'] ?? null;
                $line['LIC_CMP'] = $line['LIC_CMP'] ?? null;
                $this->connectionPsql->executeStatement($query, $line);
            }

            // If all queries are executed successfully, commit the transaction
            $this->connectionPsql->commit();
            $io->success('Composantes insérées dans la base PSQL');
        } catch (\Exception $e) {
            // If any query fails, roll back the transaction
            $this->connectionPsql->rollBack();

            // Output the error message
            throw $e;
        }
        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected  function importFormations(SymfonyStyle $io): int
    {
        $query = file_get_contents($this->projectDir.'/data/request_formation.sql');
        $stmt = $this->connectionOracle->prepare($query);
        $result = $stmt->executeQuery();
        $formationList = $result->fetchAllAssociative();
        $io->success(count($formationList).' formations récupérées depuis APOGEE');

        try {
            $io->info('Insertion des formations dans la base PSQL');

            $query = "INSERT INTO formation (id, COD_CMP, COD_VET, LIB_WEB_VET, TYPE_FORMATION, LIBELLE_TYPE_FORMATION, NIVEAU_FORMATION)
VALUES (nextval('formation_id_seq'), :COD_CMP, :COD_VET, :LIB_WEB_VET, :TYPE_FORMATION, :LIBELLE_TYPE_FORMATION, :NIVEAU_FORMATION)";

            // Start the transaction
            $this->connectionPsql->beginTransaction();

            foreach ($formationList as $line) {
                $line['LIB_WEB_VET'] = $line['LIB_WEB_VET'] ?? null;
                $line['TYPE_FORMATION'] = $line['TYPE_FORMATION'] ?? null;
                $line['LIBELLE_TYPE_FORMATION'] = $line['LIBELLE_TYPE_FORMATION'] ?? null;
                $line['NIVEAU_FORMATION'] = $line['NIVEAU_FORMATION'] ?? null;
                $this->connectionPsql->executeStatement($query, $line);
            }

            // If all queries are executed successfully, commit the transaction
            $this->connectionPsql->commit();
            $io->success('Formations insérées dans la base PSQL');
        } catch (\Exception $e) {
            // If any query fails, roll back the transaction
            $this->connectionPsql->rollBack();

            // Output the error message
            throw $e;
        }

        return Command::SUCCESS;
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected  function importCours(SymfonyStyle $io): int
    {
        $query = file_get_contents($this->projectDir.'/data/request_cour.sql');
        $stmt = $this->connectionOracle->prepare($query);
        $result = $stmt->executeQuery();
        $courList = $result->fetchAllAssociative();
        $io->success(count($courList).' cours récupérées depuis APOGEE');

        try {
            $io->info('Inserting data into database');
            $pb = $io->createProgressBar(count($courList));
            $query = "INSERT INTO cour (id, COD_ELP, COD_CMP, COD_VET, LIB_ELP, LIB_CMT_ELP, NBR_CRD_ELP, S1, S2, LANGUE_ENSEIGNEMENT, NBR_VOL_ELP)
VALUES (nextval('cour_id_seq'), :COD_ELP, :COD_CMP, :COD_VET, :LIB_ELP, :LIB_CMT_ELP, :NBR_CRD_ELP, :S1, :S2,:LANGUE_ENSEIGNEMENT,  :NBR_VOL_ELP)";

            // Start the transaction
            $this->connectionPsql->beginTransaction();
            $pb->start();
            $i = 0;
            foreach ($courList as $line) {
                $line['LIB_CMT_ELP'] = $line['LIB_CMT_ELP'] ?? null;
                $line['S1'] = $line['S1'] ?? null;
                $line['S2'] = $line['S2'] ?? null;
                $line['LANGUE_ENSEIGNEMENT'] = $line['LANGUE_ENSEIGNEMENT'] ?? null;
                $line['NBR_VOL_ELP'] = $line['NBR_VOL_ELP'] ?? null;
                $line['NBR_VOL_ELP'] = $this->numericCommatoDot($line['NBR_VOL_ELP']);
                $line['NBR_CRD_ELP'] = $this->numericCommatoDot($line['NBR_CRD_ELP']);


                $this->connectionPsql->executeStatement($query, $line);
                $i++;
                $pb->advance();
            }

            // If all queries are executed successfully, commit the transaction
            $this->connectionPsql->commit();
            $pb->finish();
            $io->success($i.' cours insérées dans la base PSQL');
        } catch (\Exception $e) {
            // If any query fails, roll back the transaction
            $this->connectionPsql->rollBack();

            // Output the error message
            throw $e;
        }

        return Command::SUCCESS;
    }

    protected function numericCommatoDot($nbr): float
    {
        return (float) str_replace(',', '.', $nbr);
    }
}
