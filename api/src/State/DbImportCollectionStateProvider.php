<?php
# api/src/State/DbImportCollectionStateProvider.php

namespace App\State;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Entity\Composante;
use App\Entity\Cour;
use Doctrine\Persistence\ManagerRegistry;

final class DbImportCollectionStateProvider implements ProviderInterface
{
    private $managerRegistry;

    public function __construct(ManagerRegistry $managerRegistry)
    {
        $this->managerRegistry = $managerRegistry;
    }

    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return in_array($resourceClass, [Composante::class, Cour::class]);
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = []): array|null|object
    {
        $resourceClass = $operation->getClass();

        $manager = $this->managerRegistry->getManagerForClass($resourceClass);
        $repository = $manager->getRepository($resourceClass);

        $page = $context['filters']['page'] ?? 1;
        $itemsPerPage = $context['filters']['itemsPerPage'] ?? 30;

        $totalItems = $repository->count([]);
        $pageCount = ceil($totalItems / $itemsPerPage);

        $books = $repository->findBy([], null, $itemsPerPage, ($page - 1) * $itemsPerPage);

        $halData = [
            '_links' => [
                'self' => [
                    'href' => "{$resourceClass}s?page={$page}&itemsPerPage={$itemsPerPage}",
                ],
            ],
            '_embedded' => [
                'items' => $books,
            ],
            'page' => $page,
            'itemsPerPage' => $itemsPerPage,
            'page_count' => $pageCount,
        ];

        return $halData;
    }
}
